/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.dao;

import br.com.twoforone.workstationapp.workstationapp.persistence.dao.interfaces.NotificationCRUD;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Notification;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Workstation;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.repositories.NotificationRepository;
import java.util.List;

/**
 * Class to execute BasicCRUD operations for the models
 *
 * @author eduardo-lima
 *
 */
public class NotificationDAO extends BaseDAO implements NotificationCRUD {

    @Override
    public void add(Notification n) throws DatabaseEx {
        NotificationRepository repo = new NotificationRepository(this.getEntityManager());
        repo.add(n);
    }

    @Override
    public List<Notification> getNotificationByWorkstation(Workstation workstation) throws DatabaseEx {
        NotificationRepository repo = new NotificationRepository(this.getEntityManager());
        return repo.findByWorkstation(workstation);
    }

    @Override
    public boolean isNotificationExists(String desc, long workstationID) throws DatabaseEx {
        NotificationRepository repo = new NotificationRepository(this.getEntityManager());
        return repo.isNotificationExists(desc, workstationID);
    }

}

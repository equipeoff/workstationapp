/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.dao.interfaces;

import br.com.twoforone.workstationapp.workstationapp.persistence.dao.DatabaseEx;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Manager;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Worker;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Workstation;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author home
 */
public interface WorkstationCRUD {

    public void add(Workstation object) throws DatabaseEx;
    
    public List<Workstation> findWorkstationByManager(Manager manager) throws DatabaseEx;

    public List<Worker> findLateWorkersAtWorkstation(long workstationID, Calendar when) throws DatabaseEx;
}

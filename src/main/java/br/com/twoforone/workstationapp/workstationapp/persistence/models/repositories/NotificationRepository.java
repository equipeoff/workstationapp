/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.models.repositories;

import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Notification;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Workstation;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * Domain Design driven
 *
 * @author eduardo-lima
 */
public class NotificationRepository {

    private final EntityManager manager;

    public NotificationRepository(EntityManager em) {
        this.manager = em;
    }

    public void add(Notification m) {
        this.manager.persist(m);
    }

    public List<Notification> findByWorkstation(Workstation workstation) {

        Query qry = this.manager.createQuery("SELECT n FROM Notification n WHERE n.workstation.id = ?1 ORDER BY n.createdAt DESC", Notification.class);
        qry.setParameter(1, workstation.getId());

        return qry.getResultList();
    }

    public boolean isNotificationExists(String desc, long workstationID) {
        Query qry = this.manager.createQuery("SELECT n FROM Notification n WHERE n.description = ?1 AND n.workstation.id = ?2", Notification.class);
        qry.setParameter(1, desc);
        qry.setParameter(2, workstationID);

        return !qry.getResultList().isEmpty();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.dao.helpers;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Encrypt
 *
 * @author home
 */
public class Encrypt {

    /**
     * Convert any string value to MD5 algorithm
     *
     * @param value String
     * @return
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static String convertStringToMd5(String value) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest mDigest;

        // Get instance 
        mDigest = MessageDigest.getInstance("MD5");

        // Convert value to a byte array
        byte[] valueMd5 = mDigest.digest(value.getBytes("UTF-8"));

        // bytes to hexa
        StringBuffer sb = new StringBuffer();
        for (byte b : valueMd5) {
            sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
        }

        return sb.toString();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.dao;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

/**
 * Base DAO to set entity manager and provide it to all subclasses
 * @author eduardo-lima
 */
public abstract class BaseDAO {

    private final EntityManager entityManager;

    public BaseDAO() {
        
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) ec.getRequest();

        this.entityManager = (EntityManager) request.getAttribute("EntityManager");
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }
}

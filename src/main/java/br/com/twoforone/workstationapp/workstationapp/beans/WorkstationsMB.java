/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.beans;

import br.com.twoforone.workstationapp.workstationapp.persistence.dao.DatabaseEx;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.NotificationDAO;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.WorkerDAO;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.WorkstationDAO;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.helpers.SessionContext;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.interfaces.NotificationCRUD;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.interfaces.WorkerCRUD;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.interfaces.WorkstationCRUD;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Manager;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Notification;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Worker;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Workstation;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.joda.time.LocalDate;

/**
 * Manager for workstation information
 *
 * @author eduardo-lima
 */
@ManagedBean
@ViewScoped
public class WorkstationsMB {

    private Manager loggedManager;

    // List all workstations available 
    private final List<Workstation> workstations = new ArrayList<>();

    // Object to manipulate current workstation (on adding, editing or reading)
    private Workstation workstation;

    @PostConstruct
    public void init() {
        this.loggedManager = SessionContext.getInstance().getLoggedManager();
        this.loadWorkstations();
    }

    /**
     * Verify what workers have missed their right markings
     *
     * @param workstationID
     */
    public void notifyLateWorkersAtWorkstation(long workstationID) {
        try {
            // Set UTC time
            Calendar utcCalendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
            // Convert localtime to UTC
            utcCalendar.setTimeInMillis(Calendar.getInstance().getTimeInMillis());

            // ALWAYS use utc time to conditional statements for EVERYTHING
            WorkstationCRUD daoWS = new WorkstationDAO();
            List<Worker> ws = daoWS.findLateWorkersAtWorkstation(workstationID, utcCalendar);

            for (Worker w : ws) {

                String desc = w.getName()
                        + " atrasou a marcação de início das "
                        + w.getSchedule().getStartAt().toString()
                        + " do dia "
                        // presentation datetime needs conversion
                        + new LocalDate(new GregorianCalendar(TimeZone.getTimeZone("America/Sao_Paulo"))).toString("dd/MM/yyyy");

                NotificationCRUD daoNT = new NotificationDAO();
                if (!daoNT.isNotificationExists(desc, workstationID)) {
                    Workstation wk = new Workstation();
                    wk.setId(workstationID);

                    Notification n = new Notification();
                    n.setDescription(desc);
                    n.setWorkstation(wk);
                    n.setCreatedAt(utcCalendar);
                    daoNT.add(n);
                }
            }
        } catch (DatabaseEx ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
        }
    }

    /**
     * Load workstations from db
     *
     * @return List of Workstations objects
     */
    private void loadWorkstations() {

        try {
            this.workstations.clear();

            // Carregar postos de trabalho da base de dados
            WorkstationCRUD dao = new WorkstationDAO();
            List<Workstation> ws = dao.findWorkstationByManager(loggedManager);

            // iterar lista e adicionar na lista da view (poderia fazer direto - mas aqui não <notificationapp>)
            for (Workstation w : ws) {
                this.workstations.add(w);
            }
        } catch (DatabaseEx ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
        }
    }

    /*
     * Init a new workstation instance
     *
     * @param e
     */
    public void newWorkstation(ActionEvent e) {
        this.workstation = new Workstation();

        this.workstation.setManager(this.loggedManager);
    }

    /**
     * Save workstation model in the database
     *
     * @param e
     */
    public void saveWorkstation(ActionEvent e) {

        try {
            if (this.workstation.getDescription().isEmpty()) {
                throw new IllegalArgumentException("É necessário informar a descrição");
            }

            WorkstationCRUD dao = new WorkstationDAO();
            dao.add(workstation);

            this.newWorkstation(null);

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Posto de trabalho adicionado", null));

            this.loadWorkstations();
        } catch (IllegalArgumentException | DatabaseEx ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
        }
    }

    /**
     * Get all notifications related to the workstation id
     *
     * @param w Workstation Id
     * @return Notification object
     */
    public List<Notification> getNotifications(Workstation w) {

        try {
            NotificationCRUD daoNT = new NotificationDAO();

            return daoNT.getNotificationByWorkstation(w);
        } catch (DatabaseEx ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
            return new ArrayList<>(); // No records
        }
    }

    /**
     * Get all workers at workstation
     *
     * @param workstationID Workstation
     * @return List of workers at specific workstation of current date
     */
    public List<Worker> getActiveWorkersNow(long workstationID) {

        List<Worker> workersInWorkstation = new ArrayList<>();

        try {
            WorkerCRUD dao = new WorkerDAO();

            Workstation w = new Workstation();
            w.setId(workstationID);

            List<Worker> wiw = dao.findWorkersInWorkstation(w, Calendar.getInstance());

            for (Worker wk : wiw) {
                workersInWorkstation.add(wk);
            }
        } catch (DatabaseEx ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
        }

        return workersInWorkstation;
    }

    /**
     * @return the workstations
     */
    public List<Workstation> getWorkstations() {
        return workstations;
    }

    /**
     * @return the workstation
     */
    public Workstation getWorkstation() {
        return workstation;
    }

    /**
     * @param workstation the workstation to set
     */
    public void setWorkstation(Workstation workstation) {
        this.workstation = workstation;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.models.repositories;

import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Marking;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Worker;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * Domain Design driven
 *
 * @author eduardo-lima
 */
public class MarkingRepository {

    private final EntityManager manager;

    public MarkingRepository(EntityManager em) {
        this.manager = em;
    }

    public void add(Marking s) {
        this.manager.persist(s);
    }
    
    public List<Marking> findMarkingsByWorker(Worker worker){
        Query qry = this.manager.createQuery("SELECT m FROM Marking m WHERE m.worker.id = ?1 ORDER BY m.createdAt DESC", Marking.class);
        qry.setParameter(1, worker.getId());
        
        return qry.getResultList();
    }

}

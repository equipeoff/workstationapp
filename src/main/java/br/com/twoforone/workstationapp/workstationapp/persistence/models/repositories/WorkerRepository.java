/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.models.repositories;

import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Manager;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Marking;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Worker;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Workstation;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 * Domain Design driven
 *
 * @author eduardo-lima
 */
public class WorkerRepository {

    private final EntityManager manager;

    public WorkerRepository(EntityManager em) {
        this.manager = em;
    }

    public void add(Worker s) {
        this.manager.persist(s);
    }

    public List<Worker> findByManager(Manager manager) {

        Query qry = this.manager.createQuery("SELECT w FROM Worker w WHERE w.manager.id = ?1 ORDER BY w.name ASC", Worker.class);
        qry.setParameter(1, manager.getId());

        return qry.getResultList();
    }

    public List<Worker> findWorkersInWorkstation(Workstation workstation, Calendar when) {

        List<Worker> resultList = new ArrayList<>();

        Query qry0 = this.manager.createQuery("SELECT w FROM Worker w WHERE w.workstation.id = ?1 ORDER BY w.name ASC", Worker.class);
        qry0.setParameter(1, workstation.getId());

        Calendar startOfDay = (Calendar) when.clone();
        startOfDay.set(Calendar.HOUR_OF_DAY, 0);
        startOfDay.set(Calendar.MINUTE, 0);
        startOfDay.set(Calendar.SECOND, 0);

        Calendar endOfDay = (Calendar) when.clone();
        endOfDay.set(Calendar.HOUR_OF_DAY, 23);
        endOfDay.set(Calendar.MINUTE, 59);
        endOfDay.set(Calendar.SECOND, 59);

        List<Worker> ws = qry0.getResultList();
        for (Worker w : ws) {

            Query qry1 = this.manager.createQuery("SELECT m FROM Marking m WHERE m.worker.id = ?1 AND (m.createdAt BETWEEN ?2 AND ?3)", Marking.class);
            qry1.setParameter(1, w.getId());
            qry1.setParameter(2, startOfDay, TemporalType.TIMESTAMP);
            qry1.setParameter(3, endOfDay, TemporalType.TIMESTAMP);

            List<Marking> ms = qry1.getResultList();

            w.setStatus((ms.size() % 2) != 0 ? "trabalhando" : "não trabalhando");

            resultList.add(w);
        }

        return resultList;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.database.models;

/**
 * Model for worker table
 *
 * @author eduardo-lima
 */
public class Worker {

    private int id;
    private Manager manager;
    private Schedule schedule;

    public Worker(int id, Manager manager, Schedule schedule, Workstation workstation, String name, String login, String password) {
        this.id = id;
        this.manager = manager;
        this.schedule = schedule;
        this.workstation = workstation;
        this.name = name;
        this.login = login;
        this.password = password;
    }
    
    public Worker(){};
    
    private Workstation workstation;
    private String name;
    private String login;
    private String password;
    private int markingTimes; // This field is not fed from db

    public Workstation getWorkstation() {
        return workstation;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the manager
     */
    public Manager getManager() {
        return manager;
    }

    /**
     * @param manager the manager to set
     */
    public void setManager(Manager manager) {
        this.manager = manager;
    }

    /**
     * @return the schedule
     */
    public Schedule getSchedule() {
        return schedule;
    }

    /**
     * @param schedule the schedule to set
     */
    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the markingTimes
     */
    public int getMarkingTimes() {
        return markingTimes;
    }

    /**
     * @param markingTimes the markingTimes to set
     */
    public void setMarkingTimes(int markingTimes) {
        this.markingTimes = markingTimes;
    }

    /**
     * @param workstation the workstation to set
     */
    public void setWorkstation(Workstation workstation) {
        this.workstation = workstation;
    }
}

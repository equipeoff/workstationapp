/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.database.models;

import java.sql.Date;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

/**
 * Model for notification table
 * @author eduardo-lima
 */
public class Notification {
    
    private int id;
    private Workstation workstation;
    private String description;
    private LocalDate date;
    private LocalTime time;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the workstation
     */
    public Workstation getWorkstation() {
        return workstation;
    }

    /**
     * @param workstation the workstation to set
     */
    public void setWorkstation(Workstation workstation) {
        this.workstation = workstation;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the date
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * @return the time
     */
    public LocalTime getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(LocalTime time) {
        this.time = time;
    }
}

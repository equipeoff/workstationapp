/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.ws.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * This is a way to other allowed applications apply and change values to the
 * database
 *
 * @author eduardo-lima
 */
@Path("/worker")
public class WorkerResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String respondAsReady() {
        return "Service is ready!";
    }

    
}

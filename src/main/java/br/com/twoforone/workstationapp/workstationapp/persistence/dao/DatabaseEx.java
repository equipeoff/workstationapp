/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.dao;

/**
 * Specific error for database operations
 *
 * @author eduardo-lima
 *
 */
public class DatabaseEx extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public DatabaseEx(String message) {
        super(message);
    }
}

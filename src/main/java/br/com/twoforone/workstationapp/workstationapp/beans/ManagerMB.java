/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.beans;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.helpers.SessionContext;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Manager;

/**
 * Manager for user information
 *
 * @author eduardo-lima
 */
@ManagedBean
@SessionScoped
public class ManagerMB {

    private Manager loggedManager;

    @PostConstruct
    public void init() {
        this.loggedManager = SessionContext.getInstance().getLoggedManager();
    }

    /**
     * @return the loggedManager
     */
    public Manager getLoggedManager() {
        return loggedManager;
    }

}

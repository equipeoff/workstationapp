/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.models.repositories;

import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Manager;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Schedule;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * Domain Design driven
 *
 * @author eduardo-lima
 */
public class ScheduleRepository {

    private final EntityManager manager;

    public ScheduleRepository(EntityManager em) {
        this.manager = em;
    }

    public void add(Schedule s) {
        this.manager.persist(s);
    }

    public List<Schedule> findByManager(Manager manager) {

        Query qry = this.manager.createQuery("SELECT s FROM Schedule s WHERE s.manager.id = ?1");
        qry.setParameter(1, manager.getId());

        return qry.getResultList();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.dao.helpers;

import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Manager;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author home
 */
public class SessionContext {

    private static SessionContext instance;

    public static SessionContext getInstance() {
        if (instance == null) {
            instance = new SessionContext();
        }
        return instance;
    }

    private SessionContext() {

    }

    private ExternalContext currentExternalContext() {
        if (FacesContext.getCurrentInstance() == null) {
            throw new RuntimeException("O FacesContext não pode ser chamado fora de uma requisição HTTP");
        } else {
            return FacesContext.getCurrentInstance().getExternalContext();
        }
    }

    public Manager getLoggedManager() {
        return (Manager) getAttribute("loggedManager");
    }

    public void setLoggedManager(Manager usuario) {
        setAttribute("loggedManager", usuario);
    }

    public void endSession() {
        currentExternalContext().invalidateSession();
    }

    public Object getAttribute(String name) {
        return currentExternalContext().getSessionMap().get(name);
    }

    public void setAttribute(String name, Object value) {
        currentExternalContext().getSessionMap().put(name, value);
    }

}

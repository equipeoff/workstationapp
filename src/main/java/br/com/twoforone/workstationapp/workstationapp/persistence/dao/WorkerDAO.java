/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.dao;

import br.com.twoforone.workstationapp.workstationapp.persistence.dao.interfaces.WorkerCRUD;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Manager;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Worker;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Workstation;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.repositories.WorkerRepository;
import java.util.Calendar;
import java.util.List;

/**
 * Class to execute BasicCRUD operations for the models
 *
 * @author eduardo-lima
 *
 */
public class WorkerDAO extends BaseDAO implements WorkerCRUD {

    @Override
    public void add(Worker worker) throws DatabaseEx {
        WorkerRepository repo = new WorkerRepository(this.getEntityManager());
        repo.add(worker);
    }

    @Override
    public List<Worker> findByManager(Manager manager) throws DatabaseEx {
        WorkerRepository repo = new WorkerRepository(this.getEntityManager());
        return repo.findByManager(manager);
    }

    @Override
    public List<Worker> findWorkersInWorkstation(Workstation workstation, Calendar when) throws DatabaseEx {
        WorkerRepository repo = new WorkerRepository(this.getEntityManager());
        return repo.findWorkersInWorkstation(workstation, when);
    }

}

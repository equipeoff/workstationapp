/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.beans;

import br.com.twoforone.workstationapp.workstationapp.persistence.dao.DatabaseEx;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.ManagerDAO;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.helpers.SessionContext;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.interfaces.ManagerCRUD;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Manager;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 * Manager of login view
 *
 * @author eduardo-lima
 */
@ManagedBean
public class AccountMB {

    private String login;

    private String name;

    private String password;

    private String password_2;

    /**
     * Register a new manager
     *
     * @return App path
     */
    public String register() {

        try {
            ManagerDAO dao = new ManagerDAO();
            // Verify if manager already exists
            if (dao.isLoginAlreadyExists(this.login)) {
                throw new IllegalArgumentException("Login já cadastrado");
            }

            // Verify if password and password_2 are equal
            if (!this.password.equals(this.password_2)) {
                throw new IllegalArgumentException("Senha de confirmação não confere");
            }

            Manager manager = new Manager();
            manager.setLogin(this.login);
            manager.setName(this.name);
            manager.setPassword(this.password);

            dao.add(manager);

            SessionContext.getInstance().setLoggedManager(manager);

            return "dashboard.xhtml";

        } catch (DatabaseEx | IllegalArgumentException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null));
            return "";
        }

    }

    /**
     * Authorize credentials
     *
     * @return Enter in the application
     */
    public String auth() {

        try {
            ManagerCRUD dao = new ManagerDAO();

            Manager m = dao.isManagerReadyToLogin(this.login, this.password);
            
            if (m == null){
                throw new IllegalArgumentException("Login ou senha incorreto, tente novamente");
            }

            SessionContext.getInstance().setLoggedManager(m);
            return "dashboard.xhtml";
        } catch (DatabaseEx | IllegalArgumentException ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
            return "";
        }
    }

    /**
     * Leave out the application
     *
     * @return index page
     */
    public String logOut() {

        SessionContext.getInstance().endSession();

        return "index.xhtml";
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the password_2
     */
    public String getPassword_2() {
        return password_2;
    }

    /**
     * @param password_2 the password_2 to set
     */
    public void setPassword_2(String password_2) {
        this.password_2 = password_2;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}

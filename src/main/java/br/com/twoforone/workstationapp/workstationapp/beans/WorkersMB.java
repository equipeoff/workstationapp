/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.beans;

import br.com.twoforone.workstationapp.workstationapp.persistence.dao.DatabaseEx;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.MarkingDAO;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.ScheduleDAO;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.WorkerDAO;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.WorkstationDAO;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.helpers.SessionContext;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.interfaces.MarkingCRUD;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.interfaces.ScheduleCRUD;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.interfaces.WorkerCRUD;
import br.com.twoforone.workstationapp.workstationapp.persistence.dao.interfaces.WorkstationCRUD;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Manager;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Marking;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Schedule;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Worker;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Workstation;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

/**
 * Manager for workers information
 *
 * @author eduardo-lima
 */
@ManagedBean
@ViewScoped
public class WorkersMB {

    // List all workers available 
    private final List<Worker> workers = new ArrayList<>();

    // Object to manipulate current workstation (on adding, editing or reading)
    private Worker worker;

    private long selectedSchedule;

    private long selectedWorkstation;

    // List all workers available 
    private final List<Schedule> schedules = new ArrayList<>();
    private final List<Workstation> workstations = new ArrayList<>();

    private Manager loggedManager;

    @PostConstruct
    public void init() {

        this.loggedManager = SessionContext.getInstance().getLoggedManager();

        this.loadWorkers(loggedManager);
        this.loadSchedules(loggedManager);
        this.loadWorkstations(loggedManager);
    }

    /**
     * Method to load all workers created by a manager
     *
     * @param m Logged Manager
     */
    private void loadWorkers(Manager m) {

        try {
            this.getWorkers().clear();

            // Carregar postos de trabalho da base de dados
            WorkerCRUD dao = new WorkerDAO();
            List<Worker> ws = dao.findByManager(loggedManager);

            // iterar lista e adicionar na lista da view (poderia fazer direto - mas aqui não <notificationapp>)
            for (Worker w : ws) {
                this.getWorkers().add(w);
            }
        } catch (DatabaseEx ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
        }
    }

    /**
     * Method to load all schedules created by a manager
     *
     * @param m Logged Manager
     */
    private void loadSchedules(Manager m) {

        try {
            this.getSchedules().clear();

            // Carregar postos de trabalho da base de dados
            ScheduleCRUD crud = new ScheduleDAO();
            List<Schedule> ss = crud.findByManager(m);

            // iterar lista e adicionar na lista da view (poderia fazer direto - mas aqui não <notificationapp>)
            for (Schedule s : ss) {
                this.getSchedules().add(s);
            }
        } catch (DatabaseEx ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
        }
    }

    /*
     * Init a new worker instance
     *
     * @param e
     */
    public void newWorker(ActionEvent e) {
        this.setWorker(new Worker());

        this.getWorker().setManager(this.loggedManager);
    }

    /**
     * Save workstation model in the database
     *
     * @param e
     */
    public void saveWorker(ActionEvent e) {

        try {
            if (this.getWorker().getLogin().isEmpty()) {
                throw new IllegalArgumentException("É necessário informar o login");
            }
            if (this.getWorker().getName().isEmpty()) {
                throw new IllegalArgumentException("É necessário informar o nome");
            }
            if (this.getWorker().getPassword().isEmpty()) {
                throw new IllegalArgumentException("É necessário informar a senha de acesso");
            }
            if (this.selectedSchedule <= 0) {
                throw new IllegalArgumentException("É necessário selecionar a jornada");
            }
            if (this.selectedWorkstation <= 0) {
                throw new IllegalArgumentException("É necessário selecionar o posto de trabalho");
            }

            Schedule s = new Schedule();
            s.setId(selectedSchedule);

            this.getWorker().setSchedule(s);

            Workstation workstation = new Workstation();
            workstation.setId(selectedWorkstation);

            this.getWorker().setWorkstation(workstation);

            WorkerCRUD dao = new WorkerDAO();
            dao.add(this.getWorker());

            this.newWorker(null);
            this.selectedSchedule = 0L;
            this.selectedWorkstation = 0L;

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Colaborador adicionado", null));

            this.loadWorkers(this.loggedManager);
        } catch (IllegalArgumentException | DatabaseEx ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
        }
    }

    public List<Marking> getMarkings(Worker w) {

        try {
            MarkingCRUD dao = new MarkingDAO();

            return dao.findByWorker(w);
        } catch (DatabaseEx ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
            return new ArrayList<>(); // No records
        }

    }

    private void loadWorkstations(Manager loggedManager) {

        try {
            this.getWorkstations().clear();

            // Carregar postos de trabalho da base de dados
            WorkstationCRUD dao = new WorkstationDAO();
            List<Workstation> w = dao.findWorkstationByManager(loggedManager);

            // iterar lista e adicionar na lista da view (poderia fazer direto - mas aqui não <notificationapp>)
            for (Workstation ww : w) {
                this.getWorkstations().add(ww);
            }
        } catch (DatabaseEx ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), null));
        }
    }

    public long getSelectedWorkstation() {
        return selectedWorkstation;
    }

    public void setSelectedWorkstation(long selectedWorkstation) {
        this.selectedWorkstation = selectedWorkstation;
    }

    /**
     * @return the selectedSchedule
     */
    public long getSelectedSchedule() {
        return selectedSchedule;
    }

    /**
     * @param selectedSchedule the selectedSchedule to set
     */
    public void setSelectedSchedule(long selectedSchedule) {
        this.selectedSchedule = selectedSchedule;
    }

    /**
     * @return the worker
     */
    public Worker getWorker() {
        return worker;
    }

    /**
     * @param worker the worker to set
     */
    public void setWorker(Worker worker) {
        this.worker = worker;
    }

    /**
     * @return the workers
     */
    public List<Worker> getWorkers() {
        return workers;
    }

    /**
     * @return the schedules
     */
    public List<Schedule> getSchedules() {
        return schedules;
    }

    public List<Workstation> getWorkstations() {
        return workstations;
    }

}

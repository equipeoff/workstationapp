/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.dao;

import br.com.twoforone.workstationapp.workstationapp.persistence.dao.interfaces.ManagerCRUD;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Manager;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Schedule;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.repositories.ManagerRepository;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.repositories.ScheduleRepository;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.Time;
import java.util.Calendar;


/**
 * 
 * @author home
 */
public class ManagerDAO extends BaseDAO implements ManagerCRUD {

    @Override
    public boolean isLoginAlreadyExists(String login) throws DatabaseEx {
        ManagerRepository repo = new ManagerRepository(this.getEntityManager());
        boolean isExists = repo.getByLogin(login) != null;
        return isExists;
    }

    @Override
    public void add(Manager m) throws DatabaseEx {
        ManagerRepository repo = new ManagerRepository(this.getEntityManager());
        repo.add(m);
        
        // Configure default settings to the manager
        Schedule s = new Schedule();
        s.setManager(m);
        s.setDescription("12/36");
        
        Calendar calendar = Calendar.getInstance();
        
        calendar.set(Calendar.HOUR_OF_DAY, 7);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        s.setStartAt(new Time(calendar.getTimeInMillis()));
        
        calendar.set(Calendar.HOUR_OF_DAY, 18);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        s.setFisnishAt(new Time(calendar.getTimeInMillis()));
        
        ScheduleRepository repo2 = new ScheduleRepository(this.getEntityManager());
        repo2.add(s);
    }

    @Override
    public Manager getByLogin(String login) throws DatabaseEx {
        ManagerRepository repo = new ManagerRepository(this.getEntityManager());
        return repo.getByLogin(login);
    }

    @Override
    public Manager isManagerReadyToLogin(String login, String password) throws DatabaseEx {
        try {
            ManagerRepository repo = new ManagerRepository(this.getEntityManager());
            return repo.isManagerReadyToLogin(login, password);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            return null;
        }
    }

}

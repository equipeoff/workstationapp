/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.dao.interfaces;

import br.com.twoforone.workstationapp.workstationapp.persistence.dao.DatabaseEx;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Manager;

/**
 *
 * @author home
 */
public interface ManagerCRUD {
    
    public boolean isLoginAlreadyExists(String login) throws DatabaseEx;
    
    public void add(Manager m) throws DatabaseEx;
    
    public Manager getByLogin(String login) throws DatabaseEx;
    
    public Manager isManagerReadyToLogin(String login, String password) throws DatabaseEx;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.models.entities;

import java.io.Serializable;
import java.sql.Time;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Schedule Model
 *
 * @author eduardo-lima
 */
@Entity
public class Schedule implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String description;

    private Time startAt;
    private Time fisnishAt;

    @ManyToOne
    private Manager manager;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the manager
     */
    public Manager getManager() {
        return manager;
    }

    /**
     * @param manager the manager to set
     */
    public void setManager(Manager manager) {
        this.manager = manager;
    }

    /**
     * @return the startAt
     */
    public Time getStartAt() {
        return startAt;
    }

    /**
     * @param startAt the startAt to set
     */
    public void setStartAt(Time startAt) {
        this.startAt = startAt;
    }

    /**
     * @return the fisnishAt
     */
    public Time getFisnishAt() {
        return fisnishAt;
    }

    /**
     * @param fisnishAt the fisnishAt to set
     */
    public void setFisnishAt(Time fisnishAt) {
        this.fisnishAt = fisnishAt;
    }
}

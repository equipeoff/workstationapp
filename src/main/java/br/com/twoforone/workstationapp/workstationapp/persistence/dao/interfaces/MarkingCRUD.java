/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.dao.interfaces;

import br.com.twoforone.workstationapp.workstationapp.persistence.dao.DatabaseEx;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Marking;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Worker;
import java.util.List;

/**
 *
 * @author home
 */
public interface MarkingCRUD {
    
    public void add(Marking marking) throws DatabaseEx;
    
    public List<Marking> findByWorker(Worker worker) throws DatabaseEx;
}

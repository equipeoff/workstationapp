/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.dao;

import br.com.twoforone.workstationapp.workstationapp.persistence.dao.interfaces.MarkingCRUD;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Marking;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Worker;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.repositories.MarkingRepository;
import java.util.List;

/**
 *
 * @author home
 */
public class MarkingDAO extends BaseDAO implements MarkingCRUD {

    @Override
    public void add(Marking marking) throws DatabaseEx {

        MarkingRepository repo = new MarkingRepository(this.getEntityManager());
        repo.add(marking);
    }

    @Override
    public List<Marking> findByWorker(Worker worker) throws DatabaseEx {
        MarkingRepository repo = new MarkingRepository(this.getEntityManager());
        return repo.findMarkingsByWorker(worker);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.dao;

import br.com.twoforone.workstationapp.workstationapp.persistence.dao.interfaces.ScheduleCRUD;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Manager;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Schedule;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.repositories.ScheduleRepository;
import java.util.List;


/**
 * Class to execute BasicCRUD operations for the models
 *
 * @author eduardo-lima
 *
 */
public class ScheduleDAO extends BaseDAO implements ScheduleCRUD {

    @Override
    public void add(Schedule schedule) throws DatabaseEx {
        ScheduleRepository repo = new ScheduleRepository(this.getEntityManager());
        repo.add(schedule);
    }

    @Override
    public List<Schedule> findByManager(Manager manager) throws DatabaseEx {
       ScheduleRepository repo = new ScheduleRepository(this.getEntityManager());
        return repo.findByManager(manager);
    }
    
}

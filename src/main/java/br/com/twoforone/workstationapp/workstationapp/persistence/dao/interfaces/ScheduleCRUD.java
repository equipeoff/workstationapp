/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.dao.interfaces;

import br.com.twoforone.workstationapp.workstationapp.persistence.dao.DatabaseEx;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Manager;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Schedule;
import java.util.List;

/**
 *
 * @author home
 */
public interface ScheduleCRUD {
    
    public void add(Schedule schedule) throws DatabaseEx;
    
    public List<Schedule> findByManager(Manager manager) throws DatabaseEx;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.dao;

import br.com.twoforone.workstationapp.workstationapp.persistence.dao.interfaces.WorkstationCRUD;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Manager;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Worker;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Workstation;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.repositories.WorkstationRepository;
import java.util.Calendar;
import java.util.List;

/**
 * Class to execute BasicCRUD operations for the models
 *
 * @author eduardo-lima
 *
 */
public class WorkstationDAO extends BaseDAO implements WorkstationCRUD{

    @Override
    public void add(Workstation object) throws DatabaseEx {

        WorkstationRepository repo = new WorkstationRepository(this.getEntityManager());
        repo.add(object);

    }

    @Override
    public List<Workstation> findWorkstationByManager(Manager manager) throws DatabaseEx {
        
        WorkstationRepository repo = new WorkstationRepository(this.getEntityManager());
        return repo.findByManager(manager);
    
    }

    @Override
    public List<Worker> findLateWorkersAtWorkstation(long workstationID, Calendar when) throws DatabaseEx {

        WorkstationRepository repo = new WorkstationRepository(this.getEntityManager());
        return repo.findLateWorkersAtWorkstation(workstationID, when);
    }
}

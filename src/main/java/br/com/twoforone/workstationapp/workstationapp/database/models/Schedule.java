/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.database.models;

import org.joda.time.LocalTime;

/**
 * Model for schedule table
 *
 * @author eduardo-lima
 */
public class Schedule {

    private int id;
    private String description;
    private LocalTime startAt;
    private LocalTime fisnishAt;
    private Manager manager;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the startAt
     */
    public LocalTime getStartAt() {
        return startAt;
    }

    /**
     * @param startAt the startAt to set
     */
    public void setStartAt(LocalTime startAt) {
        this.startAt = startAt;
    }

    /**
     * @return the fisnishAt
     */
    public LocalTime getFisnishAt() {
        return fisnishAt;
    }

    /**
     * @param fisnishAt the fisnishAt to set
     */
    public void setFisnishAt(LocalTime fisnishAt) {
        this.fisnishAt = fisnishAt;
    }

    /**
     * @return the manager
     */
    public Manager getManager() {
        return manager;
    }

    /**
     * @param manager the manager to set
     */
    public void setManager(Manager manager) {
        this.manager = manager;
    }
}

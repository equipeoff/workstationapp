/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.models.repositories;

import br.com.twoforone.workstationapp.workstationapp.persistence.dao.helpers.Encrypt;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Manager;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * Domain Design driven
 * @author eduardo-lima
 */
public class ManagerRepository {
 
    private final EntityManager manager;
    
    public ManagerRepository(EntityManager em){
        this.manager = em;
    }
    
    public void add(Manager m){
        
        try {
            m.setPassword(Encrypt.convertStringToMd5(m.getPassword()));
            
            this.manager.persist(m);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            //silent
        }
    }
    
    public void delete(Long id){
        Manager m = this.find(id);
        this.manager.remove(m);
    }
    
    public Manager update(Manager m){
        return this.manager.merge(m);
    }
    
    public Manager find(Long id){
        return this.manager.find(Manager.class, id);
    }
    
    public List<Manager> getList(){
        Query qry = this.manager.createQuery("SELECT m FROM Manager m");
        return qry.getResultList();
    }
    
    public Manager getByLogin(String login){
        Query qry = this.manager.createQuery("SELECT m FROM Manager m WHERE m.login = ?1", Manager.class);
        qry.setParameter(1, login);
        
        List<Manager> list = qry.getResultList();
        
        if (list.size() > 0){
            return list.get(0);
        }
        return null;
    }
    
    public Manager isManagerReadyToLogin(String email, String password) throws NoSuchAlgorithmException, UnsupportedEncodingException{
        
        email  = email.toLowerCase().trim();
        
        Query qry = this.manager.createQuery("SELECT m FROM Manager m WHERE m.login = ?1 AND m.password = ?2", Manager.class);
        qry.setParameter(1, email);
        qry.setParameter(2, Encrypt.convertStringToMd5(password));
    
        if (qry.getResultList().size() > 0){
            return (Manager) qry.getResultList().get(0);
        }
        return null;
    }
}

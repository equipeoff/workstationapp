/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.twoforone.workstationapp.workstationapp.persistence.models.repositories;

import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Manager;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Marking;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Schedule;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Worker;
import br.com.twoforone.workstationapp.workstationapp.persistence.models.entities.Workstation;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.joda.time.LocalTime;

/**
 * Domain Design driven
 *
 * @author eduardo-lima
 */
public class WorkstationRepository {

    private final EntityManager manager;

    public WorkstationRepository(EntityManager em) {
        this.manager = em;
    }

    public void add(Workstation m) {
        this.manager.persist(m);
    }

    public List<Workstation> findByManager(Manager manager) {

        Query qry = this.manager.createQuery("SELECT ws FROM Workstation ws WHERE ws.manager.id = ?1 ORDER BY ws.description ASC", Workstation.class);
        qry.setParameter(1, manager.getId());

        return qry.getResultList();
    }

    public List<Worker> findLateWorkersAtWorkstation(long workstationID, Calendar when) {

        List<Worker> result = new ArrayList<>();

        // Get all workers of the workstation
        Query qry = this.manager.createQuery("SELECT w FROM Worker w JOIN w.workstation ws WHERE w.workstation.id = ?1 ORDER BY w.name ASC", Worker.class);
        qry.setParameter(1, workstationID);

        Calendar startOfDay = (Calendar) when.clone();
        startOfDay.set(Calendar.HOUR_OF_DAY, 0);
        startOfDay.set(Calendar.MINUTE, 0);
        startOfDay.set(Calendar.SECOND, 0);

        Calendar endOfDay = (Calendar) when.clone();
        endOfDay.set(Calendar.HOUR_OF_DAY, 23);
        endOfDay.set(Calendar.MINUTE, 59);
        endOfDay.set(Calendar.SECOND, 59);

        List<Worker> ws = new ArrayList<>(qry.getResultList());
        for (Worker w : ws) {
            // Find for markings of each worker and order by time
            Query qry2 = this.manager.createQuery("SELECT m FROM Marking m WHERE m.worker.id = ?1 AND (m.createdAt BETWEEN ?2 AND ?3) ORDER BY m.createdAt ASC", Marking.class);
            qry2.setParameter(1, w.getId());
            qry2.setParameter(2, startOfDay);
            qry2.setParameter(3, endOfDay);
            List<Marking> hisMarkings = qry2.getResultList();

            // Find for schedules of each worker
            Query qry3 = this.manager.createQuery("SELECT s FROM Schedule s WHERE s.id = ?1", Schedule.class);
            qry3.setParameter(1, w.getSchedule().getId());
            List<Schedule> hisSchedule = new ArrayList<>(qry3.getResultList());

            boolean hasMarkingsToday = hisMarkings.size() > 0; // Verify if worker has markings today
            boolean hasMarkedAfterStart = false; // Default 

            // When has markings for today then it is necessary to compare marking time with startat time
            if (hasMarkingsToday) {
                LocalTime markingTime = new LocalTime(hisMarkings.get(0).getCreatedAt());
                LocalTime startAtTime = new LocalTime(hisSchedule.get(0).getStartAt());

                hasMarkedAfterStart = markingTime.isAfter(startAtTime);
            }

            LocalTime timeNow = new LocalTime(Calendar.getInstance());
            LocalTime startAtTIme = new LocalTime(hisSchedule.get(0).getStartAt());
            boolean startAtTimeBeforeNow = startAtTIme.isBefore(timeNow);

            // Add only workers who should have marked his time (e.g. workers with no markings until in a day or have marked after schedule time
            if ((startAtTimeBeforeNow) && ((!hasMarkingsToday) || ((hasMarkingsToday) && (hasMarkedAfterStart)))) {
                result.add(w);
            }
        }

        return result;
    }
}
